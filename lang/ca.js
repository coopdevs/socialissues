export default {
  moto: 'Troba issues socials',
  submoto: 'Fes les comunitats (i el món) més fortes gràcies al codi',
  what_is_this: "Explica'm més",
  about: 'Sobre socialissues.tech',
  view_source: 'Codi font',
  open_since: 'Obert des de',
  view_more: 'Veure més...',
  go_for_it: 'Ves-hi!',
  in: 'A',
  close: 'Tanca',
  source_licensed: 'El codi font està llicenciat amb',
  website_licensed: 'El contingut web està llicenciat amb',
  footer_join_us: "és un projecte de l'economia social, uneix-te a nosaltres!"
}
